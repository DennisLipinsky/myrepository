package HW2;

/*
    Дано натуральное число n (обычная переменная int). Рекурсивно вывести все числа от 1 до n.
(Ввод 5 -> вывод 1 2 3 4 5, делать отдельным классом и в нем метод)

 */

public class RecursionExample {

    public static void main(String[] args) {
        int number = 5;
        System.out.println("Recursive output up to " + number + ":");
        fiboNumb(number);
    }

    private static void fiboNumb (int number) {
        if (number != 0) {
            fiboNumb(number - 1);
            System.out.print(number + " ");
        }
    }

}
