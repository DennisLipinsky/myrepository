package HW2;

import java.util.ArrayList;
import java.util.List;

public class CarProcessor {
    public static void main(String[] args) {

        List <Car> cars = new ArrayList<>();
        cars.add(new Car(1,"VW", "Golf", 2002, "Black", 4000, "АА0123КП"));
        cars.add(new Car(2,"VW", "Golf", 1997, "Blue", 2500, "ВС4567НЕ"));
        cars.add(new Car(3,"Audi", "A6", 2002, "Black", 6500, "AІ5555ТМ"));
        cars.add(new Car(4,"BMW", "X1", 2006, "Silver", 10000, "AA1111AA"));
        cars.add(new Car(5,"Audi", "A3", 1997, "Green", 3000, "АЕ6846ВВ"));
        cars.add(new Car(6,"VW", "Golf", 2008, "Black", 10000, "AР0543АК"));
        cars.add(new Car(7,"VW", "Golf", 2002, "Red", 4500, "AХ6543НА"));
        cars.add(new Car(8,"Seat", "Ibiza", 2008, "Black", 8500, "ВВ1564АС"));

        System.out.println("VW list:");
        for (Car car: cars) {
            if (car.mark.equals("VW")) {
                System.out.println(car.toString());
            }
        }
        System.out.println();

        int year = 2004;
        System.out.println("Golf list older than " + year + ":");
        for (Car car: cars) {
            if (car.model.equals("Golf") && car.year < year) {
                System.out.println(car.toString());
            }
        }
        System.out.println();

        year = 2002;
        int price = 4000;
        System.out.println("Car list of " + year + " year expensive than " + price + ":");
        for (Car car: cars) {
            if (car.year == 2002 && car.price > price) {
                System.out.println(car.toString());
            }
        }
    }
}
