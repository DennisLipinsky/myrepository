/*
    Find simple numbers in limits from 2 up to 100.
    This solution was honestly borrowed by me from cyberforum.ru.
 */

public class Homework2 {
    public static void main(String[] args) {

        @SuppressWarnings({"unchecked"})

        int currentNumber, dividers;
        for (currentNumber = 2; currentNumber <= 100; currentNumber++) {
            dividers = 0;
            for (int i = 1; i <= currentNumber; i++) {
                if (currentNumber % i == 0) dividers++;
            }
            if (dividers == 2) System.out.print(currentNumber + " ");

        }
    }
}
