/*  Fibonacci row up to 20 numbers to be created.
* */

public class Homework3 {
    public static void main(String[] args) {
        int numbers = 20;
        System.out.println("Here is the Fibonacci row up to " + numbers + " element:");
        try {
            fiboNumb(numbers, 0, 1);
        }
        catch (ArithmeticException e) {
            System.out.println("Oops! Arithmetic error.");
        }
    }

    private static void fiboNumb (int numbers, int currentNumb, int sum) throws  ArithmeticException{
        System.out.print(currentNumb + " ");
        int nextNumb = sum;
        sum = currentNumb + nextNumb;
        currentNumb = nextNumb;
        if (numbers - 1 > 0) {
            fiboNumb(numbers - 1, currentNumb, sum);
        }
    }
}
