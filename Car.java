package HW2;

/*
  Создать класс по спецификации. Определить конструктор для всех полей. Создать отдельно класс процессор, который
будет выполнять определенные действия с объектамии и печатать объект. В методе main создать массив объектов и используя
класс процессор выполнить необходимые операции.

Car: id, Марка, Модель, Год выпуска, Цвет, Цена, Регистрационный номер.
Создать массив объектов. Вывести:
a) список автомобилей заданной марки;
b) список автомобилей заданной модели, которые эксплуатируются больше n лет;
c) список автомобилей заданного года выпуска, цена которых больше указанной.

 */

public class Car {
    private int id;
    protected String mark;
    protected String model;
    protected int year;
    private String color;
    protected int price;
    private String number;

    public Car(int id, String mark, String model, int year, String color, int price, String number) {
        this.id = id;
        this.mark = mark;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
        this.number = number;
    }



    @Override
    public String toString() {
        return "Car " +
                "id=" + id +
                ", mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", color='" + color + '\'' +
                ", price=" + price +
                ", number='" + number + '\'';
    }
}
